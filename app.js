const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs');
const handlebars = require('handlebars');
const app = express();

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());

const hostname = '0.0.0.0';
const port = 3000;
const { exec } = require("child_process");

const shellExec = function(cmd, callback) {
    exec(cmd, (error, stdout, stderr) => {
        if (error) {
            console.error(`exec error: ${error}`);
            return;
        }
        callback(stdout);
    });
}

app.get('/', function (req, res) {
    try {

        // let datax = fs.readFileSync(__dirname + '/config/templates/sample.json').toString()
        // let ingress = JSON.parse(datax)
        // let data = {
        //     'ingress': ingress.items[0].spec.rules[0].http.paths,
        //     'baseUrl': '34.117.236.206'
        // }
        // res.render('index', {data: data});

        shellExec('kubectl get ingress --output=json', function (ingress) {
            let ingress_json = JSON.parse(ingress)
                let ingress_path = ingress_json.items.length > 0 && ingress_json.items[0].spec.rules[0].http.paths || {};

                let data = {
                    'ingress': ingress_path,
                    'baseUrl': '34.117.236.206'
                }
                console.log("Loading...")
                res.render('index', {data: data});
        });
    } catch (e) {
        console.log(e)
        res.end("An error occured")
    }
})

app.post('/create', function (req, res) {
    console.log(req.body.project)

    let portNumber = req.body.port;
    let project = 'vscode-' + portNumber + '-' + req.body.project

    console.log("req.body.port", req.body.port)
    let pod_yml = __dirname + `/config/k8s/vscode-${portNumber}.yml`;
    let ingress_yml = __dirname + `/config/k8s/vscode-ingress-${portNumber}.yml`;

    let vscode_template = fs.readFileSync(__dirname + '/config/templates/vscode.yml').toString();
    let vscode_yml = handlebars.compile(vscode_template);
    let vscode_contents = vscode_yml({ PORT_NUMBER: portNumber, PROJECT: project});
    fs.writeFileSync(pod_yml, vscode_contents);

    let vscode_ing_template = fs.readFileSync(__dirname + '/config/templates/vscode-ingress.yml').toString();
    let vscode_ing_yml = handlebars.compile(vscode_ing_template);
    let vscode_ing_contents = vscode_ing_yml({ PORT_NUMBER: portNumber, PROJECT: project});
    fs.writeFileSync(ingress_yml, vscode_ing_contents);

    shellExec(`kubectl apply -f ${pod_yml}`, function (p) {
        shellExec(`kubectl apply -f ${ingress_yml}`, function (i) {
            console.log("Createing ingress completed", i)
            res.redirect('/');
        });
    });
})

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});